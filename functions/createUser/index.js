'use strict';

const AWS = require('aws-sdk');
const client = new AWS.DynamoDB.DocumentClient({
  region: 'eu-west-1',
});
const gateway = new AWS.APIGateway({
  region: 'eu-west-1',
});

const getResponse = (statusCode, body) => ({
  statusCode,
  body: JSON.stringify({ message: body }),
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  },
});

exports.handle = (event, context, callback) => {
  const { body } = event;
  const { id, domains } = JSON.parse(body);

  const queryParams = {
    TableName: 'clients',
    Key: {
      id,
    },
  };

  const params = {
    TableName: 'clients',
    Item: {
      id,
      domains,
    },
  };

  const apiKeyParams = {
    customerId: id,
    description: 'An API key for ' + id,
    enabled: true,
    name: id,
    stageKeys: [{
          restApiId: id,
          stageName: 'beta'
        }],
    value: 'foobar'
  }

  gateway.createApiKey(apiKeyParams, (err, data) => {
    if(err) context.succeed(getResponse(500, err))
    context.succeed(getResponse(200, data));
  })

  // client.get(queryParams, (err, data) => {
  //   const clientExists = data.hasOwnProperty('Item');
  //   if (clientExists) {
  //     context.succeed(getResponse(409, 'Client already exists'));
  //   } else {
  //     client.put(params, (error, client) => {
  //       gateway.createApiKey(apiKeyParams, (errors, key) => {
  //         context.succeed(getResponse(200, key));
  //       });
  //     });
  //   }
  // });
};
