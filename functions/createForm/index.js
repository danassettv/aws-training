'use strict';

const AWS = require('aws-sdk');
const client = new AWS.DynamoDB.DocumentClient({
  region: 'eu-west-1',
});
const s3 = new AWS.S3({
  accessKey: 'AKIAJCGKXBKDVNBXP2MA',
  secretKey: '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',
});

const getResponse = (statusCode, body) => ({
  statusCode,
  body: JSON.stringify({ message: body }),
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  },
});

const generateRandomNumber = () =>
  Math.random()
    .toString(36)
    .substring(2) + new Date().getTime().toString(36);

const getScript = (id, fields) => `
/**
 ** Platform Asset TV - 1.0.0
 ** Embed Asset TV content into your existing applications
 ** Copyright 2016 Asset TV
 **/
function onUploadDone(e){document.querySelector(".camera-embed .uploading-banner").style.display="none";var t="https://atv-addpipe-eu.s3-eu-west-1.amazonaws.com/"+e+".mp4";document.querySelector('input[name="video"]').value=t}function onClickUpload(){document.querySelector(".camera-embed .uploading-banner").style.display="block"}var Camera=function(){function e(e){this.checkOptions(e)&&(this.client=e.client,this.container=e.containerId,this.fields=e.fields,this.embed())}return e.prototype.checkOptions=function(e){var t=e.client,r=e.containerId,a=e.fields;if(!t)throw Error('Please specify a "client" property');if(!r)throw Error('Please specify a "containerId" property');if(!a)throw Error("Please specify some form fields");return!0},e.prototype.appendStyles=function(){var e=document.getElementsByTagName("head")[0],t=".camera-embed { font-family: Arial, Helvetica, sans-serif; width: 100%; height: 100%;}";t+=".camera-embed * {box-sizing: border-box;}",t+=".camera-embed label {display: block; text-transform:capitalize;margin-bottom:.5rem}",t+=".camera-embed .capture-form-group {margin-bottom: 1rem;}",t+=".camera-embed input {width:100%;padding:.5rem .75rem;font-size:1rem;line-height:1.25;color:#464a4c;background-color:#fff;background-image:none;border:1px solid #CCC;border-radius:3px;outline:0}",t+=".camera-embed select {width:100%;padding:.5rem .75rem;height: calc(2.25rem + 2px);;font-size:1rem;line-height:1.25;color:#464a4c;background-color:#fff;background-image:none;border:1px solid #CCC;border-radius:3px;outline:0}",t+=".camera-embed .capture-form-group-error {border-color:red}",t+=".camera-embed form {margin-top:20px}",t+=".camera-embed textarea {width:100%;padding:.5rem .75rem;font-size:1rem;line-height:1.25;color:#464a4c;background-color:#fff;background-image:none;border:1px solid #CCC;border-radius:3px;outline:0}",t+='.camera-embed input[type="submit"] {background:#0077b5;color:#FFF;width:100%;padding:1rem .75rem;font-size:1rem;border:none;cursor:pointer}',t+='.camera-embed input[type="submit"]:hover {background-color: #005e91}',t+=".camera-embed .uploading-banner {margin-top:20px;padding:10px;background:#CCC;color:#6B6B6B;text-align:center;display:none}";var r=document.createElement("style");r.type="text/css",r.styleSheet?r.styleSheet.cssText=t:r.appendChild(document.createTextNode(t));var a=document.querySelector("link[rel='stylesheet']");a||e.appendChild(r),e.insertBefore(r,a)},e.prototype.generateRandomNumber=function(){return Math.round(1e4*Math.random())},e.prototype.getContainer=function(){return document.getElementById(this.container)},e.prototype.getSubmitButton=function(){var e=document.createElement("input");return e.type="submit",e.textContent="Submit",e},e.prototype.createHiddenField=function(){var e=document.createElement("input");return e.setAttribute("name","video"),e.hidden=!0,e},e.prototype.createHoneypotField=function(){var e=document.createElement("input");return e.name="bot-field",e.style.display="none",e},e.prototype.createVideoCapture=function(){var e=document.createElement("div");e.id="hdfvr-content";var t=this.getContainer();t.appendChild(e),window.size={width:t.offsetWidth,height:.75*t.offsetWidth},window.flashvars={qualityurl:"avq/720p.xml",accountHash:"0d7a6a9b2a0d8c2384c9eded6311beb0",eid:2,showMenu:"true",mrt:120,sis:0,asv:1,mv:0};var r=document.createElement("script");r.type="text/javascript",r.async=!0,r.src=("https:"==document.location.protocol?"https://":"http://")+"s1.addpipe.com/1.3/pipe.js";var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(r,a)},e.prototype.createForm=function(){var e=this.getContainer(),t=document.createElement("form");t.addEventListener("submit",this.handleSubmission.bind(this)),t.action="thank-you",t.method="post",t.id=this.client+"-form",t.name=this.client+"-form",t.setAttribute("netlify",""),t.setAttribute("netlify-honeypot","bot-field"),t.appendChild(this.createHoneypotField()),t.appendChild(this.createHiddenField());var r=this;Object.keys(this.fields).forEach(function(e){var a=r.fields[e],n=a.label,o=a.type,i=document.createElement("div");i.classList.add("capture-form-group");var d=r.generateRandomNumber(),c=document.createElement("label");c.setAttribute("for",e+"-"+d),c.textContent=n;var l=document.createElement("span");if(l.textContent=" *",l.style.color="red",c.appendChild(l),i.appendChild(c),"select"!==o){var s="textarea"==o?document.createElement("textarea"):document.createElement("input");s.addEventListener("keyup",r.validateInput),s.setAttribute("name",e),s.setAttribute("id",e+"-"+d),s.setAttribute("type",o),s.required=!0,s.dataset.valid=!1,i.appendChild(s)}else{var p=document.createElement("select");p.setAttribute("name",e),p.setAttribute("id",e+"-"+d),p.required=!0;var u=r.fields[e].options,m=document.createElement("option");m.textContent="Please select...",m.setAttribute("value",""),m.disabled=!0,m.selected=!0,p.appendChild(m),p.addEventListener("change",r.validateInput),p.dataset.valid=!1,u.forEach(function(e){var t=document.createElement("option");t.setAttribute("value",e),t.textContent=e,p.appendChild(t)}),i.appendChild(p)}t.appendChild(i)}),t.appendChild(this.getSubmitButton()),e.appendChild(t)},e.prototype.createUploadingBanner=function(){var e=document.createElement("div");e.classList.add("uploading-banner"),e.textContent="Uploading your Video",this.getContainer().appendChild(e)},e.prototype.validateInput=function(e){var t=e.target;e.target.value?(t.classList.contains("capture-form-group-error")&&t.classList.remove("capture-form-group-error"),t.dataset.valid=!0):(t.classList.contains("capture-form-group-error")||t.classList.add("capture-form-group-error"),t.dataset.valid=!1)},e.prototype.handleSubmission=function(e){this.validateForm(e)},e.prototype.validateForm=function(e){var t=(document.querySelector('form[name="submit-recording"]'),document.querySelectorAll('[data-valid="false"]'));if(0===t.length)return!!document.querySelector('input[name="video"]').value||(alert("Please record a video"),void e.preventDefault());t.forEach(function(e){e.classList.contains("capture-form-group-error")||e.classList.add("capture-form-group-error")}),e.preventDefault()},e.prototype.embed=function(e){this.getContainer().classList.add("camera-embed"),this.appendStyles(),this.createVideoCapture(),this.createUploadingBanner(),this.createForm()},e}();
new Camera({
  client: '${id}',
  containerId: 'camera',
  fields: ${JSON.stringify(fields)},
});
`;

exports.handle = (event, context, callback) => {
  const { body } = event;
  const { id, fields } = JSON.parse(body);

  const randomId =
    Math.random()
      .toString(36)
      .substring(2) + new Date().getTime().toString(36);

  const formId = generateRandomNumber();
  const scriptId = generateRandomNumber();

  const params = {
    TableName: 'forms',
    Item: {
      id: formId,
      fields,
      clientId: id,
      scriptId,
    },
  };

  const scriptParams = {
    TableName: 'scripts',
    Item: {
      id: scriptId,
      formId,
      clientId: id,
    },
  };

  const bucketParams = {
    Bucket: 'pulse-cam-bucket',
    Key: scriptId + '.js',
    Body: getScript(id, fields),
  };

  client.put(params, (err, data) => {
    client.put(scriptParams, (err, script) => {
      s3.upload(bucketParams, (err, data) => {
        context.succeed(getResponse(200, data.Location));
      });
    });
  });
};
