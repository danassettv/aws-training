'use strict';

const AWS = require('aws-sdk');
const client = new AWS.DynamoDB.DocumentClient({
  region: 'eu-west-1',
});

const getResponse = (statusCode, body) => ({
  statusCode,
  body: JSON.stringify({ message: body }),
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  },
});

exports.handle = (event, context, callback) => {
  const { queryStringParameters: { id } } = event;

  const params = {
    TableName: 'clients',
    Key: {
      id,
    },
  };

  client.get(params, (err, data) => {
    const clientExists = data.hasOwnProperty('Item');
    if (clientExists) {
      context.succeed(getResponse(200, data));
    } else {
      context.succeed(getResponse(404, 'Client Not Found'));
    }
  });
};
