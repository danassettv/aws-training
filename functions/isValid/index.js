'use strict';

const AWS = require('aws-sdk');
const client = new AWS.DynamoDB.DocumentClient({
  region: 'eu-west-1',
});

const getResponse = (statusCode, body) => ({
  statusCode,
  body: JSON.stringify({ message: body }),
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  },
});

exports.handle = (event, context, callback) => {
  const { queryStringParameters: { id, domain } } = event;

  const params = {
    TableName: 'clients',
    Key: {
      id,
    },
  };

  client.get(params, (err, data) => {
    const clientExists = data.hasOwnProperty('Item');
    if (clientExists) {
      const { Item: { domains } } = data;
      domains.forEach(storedDomain => {
        if (domain.replace('www.', '') === storedDomain) {
          context.succeed(getResponse(200, 'Access Allowed'));
        }
      });
      context.succeed(getResponse(401, 'Unauthorised'));
    } else {
      context.succeed(getResponse(404, 'Client Not Found'));
    }
  });
};
